## Zimbra Letsencrypt

Ansible playbook to renew letsencrypt certificate and deploy it on Zimbra mail server.

Reference: https://wiki.zimbra.com/wiki/Installing_a_LetsEncrypt_SSL_Certificate

## Requirements

- rsync package is needed on all hosts

- python cryptography module is needed on ansible host and zimbra hosts

To install python cryptography module: 

`python3 -m pip install cryptography`

If above command error, try to upgrade pip module first using below command:

`python3 -m pip install --upgrade pip`

and retry the command:

`python3 -m pip install cryptography`

- ansible community.crypto module is needed on ansible host

To install ansible community.crypto module: 

`ansible-galaxy collection install community.crypto`

## Usage

Please adjust your directory path variables first before running the playbook.

Run playbook using extra vars for domain_name: 

`ansible-playbook zmcertupdate.yml --extra-vars domain_name=example.com `

Run playbook with domain_name variables hardcoded on the zmcertupdate.yml file

`ansible-playbook zmcertupdate.yml`

Run playbook to check the new letsencrypt certificate is valid for next 89 days:

`ansible-playbook zmcertcheck.yml`
